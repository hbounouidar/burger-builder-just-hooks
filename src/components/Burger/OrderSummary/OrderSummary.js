import React from 'react';

import Aux from '../../../hoc/Auxi/Auxi';
import Button from '../../UI/Button/Button';

const orderSummary = props => { //doesnt ve to be class


    
        const ingredientSummary = Object.keys(props.ingredients)
        .map(igKey => {
            return (<li key={igKey}> 
                        <span style={{textTransform : 'capitalize'}}>{igKey}</span>
                         : {props.ingredients[igKey]}
                    </li>);
        });
        return (
            <Aux>
                <h3>Your Order</h3>
                <p>A delicous burger with following :</p>
                <ul>
                    {ingredientSummary}
                </ul>
                <p><strong>Total Price :</strong> {props.price.toFixed(2)}</p>
                <p>Continue to checkout ?</p>
                <Button btnType="Danger" clicked={props.purchaseCancelled}>CANCEL</Button>
                <Button btnType="Success" clicked={props.purchaseContinued}>CONTINUE</Button>
            </Aux>
        );
    
} 
export default orderSummary;