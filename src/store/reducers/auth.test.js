import reducer  from './auth';
import * as actionTyps from '../actions/actionTypes';

describe('auth reducer', ()=> {

    it('should return the initial state', ()=> {
        expect(reducer(undefined, {})).toEqual({
            token : null,
            userId : null,
            error : null,
            loading : null,
            authRedirectPath : '/'
        })
    });

    it('should store the token upon login', ()=> {
        expect(reducer({
            token : null,
            userId : null,
            error : null,
            loading : false,
            authRedirectPath : '/'
        }, {
            type : actionTyps.AUTH_SUCCESS,
            idToken : 'my-token',
            userId : 'my-user-id'
        })).toEqual({
            token : 'my-token',
            userId : 'my-user-id',
            error : null,
            loading : false,
            authRedirectPath : '/'
        })
    });

    


})