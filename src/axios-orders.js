import axios from 'axios';

const instance = axios.create ({
    baseURL : 'https://react-build-burger-d3329.firebaseio.com'
});


export default instance;